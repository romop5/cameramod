#ifndef STRUCTURES
#define STRUCTURES

struct Vector3D
{
	float x, y, z;
};

struct Vector2D
{
	float x, y;
};

struct Point2D
{
	int x,y;
};
#endif