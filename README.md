# CameraMod (Mafia:The city of Lost Heaven) #

CameraMod is a fan modification for game called Mafia:The city of Lost Heaven. It is devoted to server as a tool for movie makers and screenshoters. It allows its users to use free camera mod, to create camera paths and then move camera along them, to hide game GUI and to control game speed (e.g. slow motion effect).

This mod was originally coded by Romop5, the author of Lost Heaven Multiplayer (http://lh-mp.eu) in November 2014.  

### How to compile & run app ###

* Clone whole repo
* Open CameraMod.sln in Visual Studio 2013 or higher
* Compile whole project as Release
* At the end you will get CameraMod.dll and Injector.exe
* Copy them into directory with Game.exe
* Enjoy

### Why is this project open-source? ###

* This project shows how ingame modification is built, so you can learn something new
* If you feel that something is wrong or something could be even better, you can take a part in developing and suggest changes

### What licence does it belong to ? ###

This project uses beerware licence. Do whatever you want with it, but when you meet me, buy me a beer, please :)
